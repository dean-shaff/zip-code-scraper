#!/usr/bin/env node
const fs = require('fs')

const commandLineArgs = require('command-line-args')
const getUsage = require('command-line-usage')
const cliProgress = require('cli-progress');
const cheerio = require('cheerio')
const request = require('request-promise-native')

const baseURL = 'https://worldpostalcode.com'

const wait = function (milliseconds) {
  return new Promise ((resolve, reject) => {
    setTimeout(resolve, milliseconds)
  })
}


const Throttler = function (maxRequests, maxTimeout, options) {
  this.maxRequests = maxRequests
  this.maxTimeout = maxTimeout

  this.progressBar = new cliProgress.SingleBar(
    {format: 'progress [{bar}] {percentage}% | ETA: {eta}s | {value}/{total}'},
    cliProgress.Presets.shades_classic)

  if (options != undefined) {
    if ('progressBar' in options) {
      this.progressBar = options.progressBar
    }
  }

  this.throttle = async function* (promiseFactories) {
    let nBatches = Math.ceil(promiseFactories.length / this.maxRequests)
    this.progressBar.start(nBatches, 0)
    for (let idx=0; idx<nBatches; idx++) {
      let slice = promiseFactories.slice(idx*this.maxRequests, (idx + 1)*this.maxRequests)
      // yield (await Promise.all(slice.map(f => f())))
      yield Promise.all(slice.map(f => f()))
      await wait(this.maxTimeout)
      this.progressBar.increment()
    }
    this.progressBar.stop()
  }
}

const extractLocalities = function (html) {
  const $ = cheerio.load(html)
  let hrefs = $('.regions').first().find('a').map((idx, elem) => {
    let href = $(elem).attr('href')
    let name = $(elem).text()
    return {'name': name, 'href': href}
  }).get()
  return hrefs
}

const extractZipCodes = function (html) {
  const $ = cheerio.load(html)
  let codes = $('.codes .unit').map((idx, elem) => {
    let name = $(elem).find('.place').text()
    let code = $(elem).find('.code').text()
    code = code.split(' ')
    if (code.length === 1) {
      code = code[0]
    }
    return {
      'name': name,
      'code': code
    }
  }).get()
  return codes
}

const testForEnd = function (html) {
  const $ = cheerio.load(html)
  if ($('.codes').prev().prev().is($('.regions').first())) {
    return false
  }
  return true
}


const findZipCodesSerial = async function (baseURL, startingURL) {
  var _findZipCodesSerial = async function (url, subResult) {
    console.log(`_findZipCodesSerial: url=${url}`)
    let html = await request(url)
    let zipCodes = extractZipCodes(html)
    if (zipCodes.length === 0 || ! testForEnd(html)) {
      let hrefs = extractLocalities(html)
      for (let idx=0; idx<hrefs.length; idx++) {
        subResult[hrefs[idx].name] = {}
        await _findZipCodesSerial(
          `${baseURL}${hrefs[idx].href}`, subResult[hrefs[idx].name])
        await wait(100)
      }
    } else {
      for (let idx=0; idx<zipCodes.length; idx++) {
        subResult[zipCodes[idx].name] = zipCodes[idx].code
      }
    }
  }
  var results = {}
  await _findZipCodesSerial(startingURL, results)
  return results
}

const findZipCodesBatched = async function (
  baseURL, startingURL, maxRequests, maxTimeout
) {
  const extractLocalitiesAsync = async function (html) {
    return extractLocalities(html)}
  const extractZipCodesAsync = async function (html) {
    return extractZipCodes(html)}

  const extractZipCodesOrLocalitiesAsync = async function (html) {
    let localities = extractLocalities(html)
    if (localities.length === 0 ) {
      return extractZipCodes(html)
    }
    return localities
  }


  const throttler = new Throttler(maxRequests, maxTimeout)
  throttler.progressBar.options.format = 'States [{bar}] {percentage}% | ETA: {eta}s | {value}/{total}'

  var results = {}

  var states = await request(startingURL).then(extractLocalitiesAsync)
  var statesBatch = new Array(states.length)
  for (let idx=0; idx<states.length; idx++) {
    statesBatch[idx] = () => {
      return request(
        baseURL + states[idx].href
      ).then(extractZipCodesOrLocalitiesAsync)
    }
    results[states[idx].name] = {}
  }
  var stateLocalities = []

  for await (const stateLocality of throttler.throttle(statesBatch)) {
    stateLocalities.push(stateLocality)
  }

  stateLocalities = stateLocalities.reduce(
    (accum, val) => {return accum.concat(val)}, [])

  var alreadyDoneIdx = []
  var countiesBatch = []
  for (let idx=0; idx<stateLocalities.length; idx++) {
    let stateLocality = stateLocalities[idx]

    // there are a few (the Marshall Islands, in particular) cases
    // where there are no counties in a state/territory, so we need to note that
    if (stateLocality.length > 0) {
      if ('code' in stateLocality[0]) {
        alreadyDoneIdx.push(idx)
        let subResult = {}
        for (let idy=0; idy<stateLocality.length; idy++) {
          results[states[idx].name][stateLocality[idy].name] = stateLocality[idy].code
        }
        continue
      }
    }

    for (let idy=0; idy<stateLocality.length; idy++) {
      countiesBatch.push(
        () => {
          return request(baseURL + stateLocality[idy].href).then(extractZipCodesAsync)}
      )
    }
  }

  throttler.progressBar.options.format = 'Counties [{bar}] {percentage}% | ETA: {eta}s | {value}/{total}'

  var countyZipCodes = []
  for await (const zipCode of throttler.throttle(countiesBatch)) {
    countyZipCodes.push(zipCode)
  }
  countyZipCodes = countyZipCodes.reduce(
    (accum, val) => {return accum.concat(val)}, [])

  for (let idx=0; idx<stateLocalities.length; idx++) {
    if (alreadyDoneIdx.includes(idx)) {
      continue
    }
    let stateLocality = stateLocalities[idx]
    for (let idy=0; idy<stateLocality.length; idy++) {
      let codes = countyZipCodes.shift()
      let subResult = {}
      for (let idz=0; idz<codes.length; idz++) {
        subResult[codes[idz].name] = codes[idz].code
      }
      results[states[idx].name][stateLocality[idy].name] = subResult
    }
  }
  return results
}


const main = async function () {
  const optionDefinitions = [
    {
      name: 'max-requests', alias: 'r',
      type: Number,
      defaultValue: 10,
      description: "Maximum number of requests to make at a time. Defaults to 10"
    },
    {
      name: 'timeout', alias: 't',
      type: Number,
      defaultValue: 200,
      description: "Timeout between requests. Defaults to 200 ms"
    },
    {
      name: 'help', alias: 'h',
      type: Boolean,
      defaultValue: false,
      description: "Print Help message and exit"
    }
  ]
  const sections = [
    {
      header: 'US Zip Code Scraper',
      content: 'Scrape all US zip codes from https://worldpostalcode.com.'
    },
    {
      header: 'Options',
      optionList: optionDefinitions
    },
  ]
  const options = commandLineArgs(optionDefinitions)
  if (options.help) {
    console.log(getUsage(sections))
    return
  }
  // let codes = await findZipCodesSerial(baseURL, baseURL + "/united-states/")
  // fs.writeFileSync('codes.json', JSON.stringify(codes))
  let codes = await findZipCodesBatched(
      baseURL, baseURL + "/united-states/", options['max-requests'], options['timeout'])
  fs.writeFileSync('codes.batched.json', JSON.stringify(codes))
}

main().catch(err => {console.error(err)})
