## 1.0.1
- Add `"bin"` script to package.json

## 1.0.2
- Fix bug in main.js where it wouldn't run from global context

## 1.1.0
- Add CLI to main.js
