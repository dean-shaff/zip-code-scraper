const path = require('path')
const fs = require('fs')
const readline = require('readline')

const {google} = require('googleapis')

// If modifying these scopes, delete token.json.
// const SCOPES = ['https://www.googleapis.com/auth/spreadsheets.readonly']
const SCOPES = [
  'https://www.googleapis.com/auth/drive',
  'https://www.googleapis.com/auth/spreadsheets'
]
const TOKEN_PATH = 'token.json'

/**
 * Create an OAuth2 client with the given credentials, and then execute the
 * given callback function.
 * @param {Object} credentials The authorization client credentials.
 */
async function authorize(credentials) {
  const {client_secret, client_id, redirect_uris} = credentials.installed
  const oAuth2Client = new google.auth.OAuth2(
      client_id, client_secret, redirect_uris[0])

  return new Promise((resolve, reject) => {
    try {
      var tokenContents = fs.readFileSync(TOKEN_PATH)
      var token = JSON.parse(tokenContents)
      oAuth2Client.setCredentials(token)
      resolve(oAuth2Client)
    } catch (err) {
      getNewToken(oAuth2Client).then(resolve)
    }
  })
}

/**
 * Get and store new token after prompting for user authorization, and then
 * execute the given callback with the authorized OAuth2 client.
 * @param {google.auth.OAuth2} oAuth2Client The OAuth2 client to get token for.
 */
function getNewToken(oAuth2Client) {
  const rlQuestion = (rl, input) => {
    return new Promise((resolve, reject) => {
      rl.question(input, resolve)
    })
  }

  const oAuth2ClientGetToken = (oAuth2Client, code) => {
    return new Promise((resolve, reject) => {
      oAuth2Client.getToken(code, (err, token) => {
        if (err) {
          reject(err)
          return
        }
        resolve(token)
      })
    })
  }

  const authUrl = oAuth2Client.generateAuthUrl({
    access_type: 'offline',
    scope: SCOPES,
  })
  console.log('Authorize this app by visiting this url:', authUrl)
  const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout,
  })

  return rlQuestion(
    rl, 'Enter the code from that page here: '
  ).then((code) => {
    rl.close()
    return code
  }).then((code) => {
    return oAuth2ClientGetToken(oAuth2Client, code)
  }).then((token) => {
    oAuth2Client.setCredentials(token)
    fs.writeFileSync(TOKEN_PATH, JSON.stringify(token))
    console.log('Token stored to', TOKEN_PATH)
    return oAuth2Client
  })
}

const credentialsPath = path.join(__dirname, 'credentials.json')
const id = "14DPb66xJFRGzwK5fjL2TaEoYoiDwe22UmBHgFemfA_M"

const loadZipCodeData = function (path) {
  let contents = fs.readFileSync(path)
  return JSON.parse(contents)
}

const transformLocalityData = function (data) {
  const alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ".split('')
  var resource = []

  const addLastColumns = (obj) => {
    var col0 = []
    var col1 = []
    for (let key in obj) {
      let val = obj[key]
      if (val.constructor !== Array) {
        val = [val]
      }
      let col0sub = new Array(val.length).fill('')
      col0sub[0] = key
      let col1sub = val
      col0 = col0.concat(col0sub)
      col1 = col1.concat(col1sub)
    }
    return [col0, col1]
  }

  const addData = (obj) => {
    let keys = Object.keys(obj)
    if (obj[keys[0]].constructor !== Object) {
      return addLastColumns(obj)
    } else {
      let res = addData(obj[keys[0]])
      let col0 = new Array(res[0].length).fill('')
      col0[0] = keys[0]
      for (let idx=1; idx<keys.length; idx++) {
        let subRes = addData(obj[keys[idx]])
        for (let idy=0; idy<res.length; idy++) {
          res[idy] = res[idy].concat(subRes[idy])
        }
        let subCol0 = new Array(subRes[0].length).fill('')
        subCol0[0] = keys[idx]
        col0 = col0.concat(subCol0)
      }
      res.unshift(col0)
      return res
    }
  }

  var flat = addData(data)
  var resource = flat.map((elem, idx) => {
    let letter = alphabet[idx]
    return {
      majorDimension: 'COLUMNS',
      values: [elem],
      range: `${letter}1:${letter}${elem.length + 1}`
    }
  })

  return resource
}


const main = async function () {
  const zipCodeDataPath = path.join(__dirname, 'codes.batched.json')
  const zipCodeData = loadZipCodeData(zipCodeDataPath)

  const cred = JSON.parse(fs.readFileSync(credentialsPath))
  const auth = await authorize(cred)
  const drive = google.drive({version: 'v3', auth})
  const sheets = google.sheets({version: 'v4', auth})
  // const resource = {
  //   properties: {
  //     title: "US Zip Codes",
  //   }
  // }
  // const resp = await sheets.spreadsheets.create({resource})
  // console.log(resp)
  // const resp = await sheets.spreadsheets.get({spreadsheetId: id})
  // var alabamaData = zipCodeData['Alabama']
  // var data = transformLocalityData(alabamaData)
  var data = transformLocalityData(zipCodeData)
  var updateRequest = {
    spreadsheetId: id,
    resource: {
      valueInputOption: 'RAW',
      includeValuesInResponse: false,
      'data': data
    }
  }
  const resp = await sheets.spreadsheets.values.batchUpdate(updateRequest)
  // console.log(resp)

  // const files = await drive.files.list({
  //   pageSize: 10,
  //   fields: 'nextPageToken, files(id, name)',
  // })
  // const files2 = await drive.files.list({
  //   pageSize: 10,
  //   nextPageToken: files.data.nextPageToken,
  //   fields: 'nextPageToken, files(id, name)'
  // })
  //
  // console.log(files.data.files)
  // console.log(files2.data.files)

}

main().catch(err => {console.error(err)})
