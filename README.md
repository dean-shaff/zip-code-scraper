## Zip Code Scraper

Scrape all US Zip Codes from https://worldpostalcode.com.

### Usage

```
npm install zip-code-scraper -g
scrape-us-zip-codes
```

If you've cloned the repo, you can do the following:

```
node main.js
```

This creates a codes.json file with all the zip codes, ordered by state and county.
